﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            // https://superuser.com/questions/192327/how-can-i-record-sound-from-the-windows-command-line
            string ano = System.DateTime.Today.Year.ToString(); // System.DateTime.Today.Year.ToString("dd/MM/yyyy");
            string mes = System.DateTime.Today.Month.ToString();  // System.DateTime.Today.Month.ToString("dd/MM/yyyy");//
            string dia = System.DateTime.Today.Day.ToString();    // System.DateTime.Today.ToString("dd/MM/yyyy");

            string hhmmss = DateTime.Now.ToString("hh:mm:ss").Replace(":","");

            string strCmdText;
            strCmdText = $"/C %HOMEDRIVE%%HOMEPATH%" + @"\rec" + @"\fmedia.exe " + "--record --out=%HOMEDRIVE%%HOMEPATH%" + @"\to" + @"\Grabaciones" + @"\" + $"rec_{ano}-{sumarCeroFecha(mes)}-{sumarCeroFecha(dia)}_{hhmmss}.wav";
            //Console.WriteLine(strCmdText);
            //Console.ReadLine();
            System.Diagnostics.Process.Start("CMD.exe", strCmdText);
        }

        public static string sumarCeroFecha(string MesDia)
        {
            if (MesDia.Length == 1)
            {
                return "0" + MesDia;
            }
            else
            {
                return MesDia;
            }

        }
    }
}
